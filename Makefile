NPM_VERSION=latest

submodules:
	git submodule init && git submodule update --recursive

npm_install:
	docker run --rm -v $(CURDIR)/logdna-agent:/src -w /src node:$(NPM_VERSION) npm install

help:
	docker run --rm -v $(CURDIR)/logdna-agent:/src -w /src node:$(NPM_VERSION) node index.js --help

start: logdna-agent/node_modules
	docker run --rm -v $(CURDIR)/logs:/logs -v $(CURDIR)/configuration/logdna.conf:/etc/logdna.conf -v $(CURDIR)/logdna-agent:/src -w /src node:$(NPM_VERSION) node index.js 

bash: logs logdna-agent/node_modules
	docker run --rm -it -v $(CURDIR)/logs:/logs -v $(CURDIR)/configuration/logdna.conf:/etc/logdna.conf -v $(CURDIR)/logdna-agent:/src -w /src node:$(NPM_VERSION) bash

create_logs: logs
	docker run -v $(CURDIR)/logs:/logs -w /logs --rm  mingrammer/flog --delay 2 -t log -o apache.log -w 

logs:
	mkdir logs

logdna-agent/node_modules:
	docker run --rm -v $(CURDIR)/logdna-agent:/src -w /src node:$(NPM_VERSION) npm install
