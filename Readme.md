# LaogDNA Agent

## About

This repo is a test setup for LogDNA. 

We are going to run two containers:
   
 - `node` - with the logdna-agent

 - `flog` - with the log generator 

Both containers share ./logs directory


## Step 1 - Init submodule

```
make submodules
```

## Step 2 - Prepare configuration

```
cp configuration/logdna.conf.template configuration/logdna.conf
```

Add ingestion key into `configuration/logdna.conf`


## Step 3 - Starting agent

```
make start 
```

## Step 4 - Create some logs

This will populate some fake log entries into ./logs/apache.log

```
make create_logs
```

Test
